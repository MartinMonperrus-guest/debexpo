# French translations for debexpo.
# Copyright (C) 2019 ORGANIZATION
# This file is distributed under the same license as the debexpo project.
#
# Baptiste BEAUPLAT <lyknode@cilg.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: debexpo 0.0.0\n"
"Report-Msgid-Bugs-To: support@mentors.debian.net\n"
"POT-Creation-Date: 2019-05-31 11:42+0200\n"
"PO-Revision-Date: 2019-04-16 17:25+0200\n"
"Last-Translator: Baptiste BEAUPLAT <lyknode@cilg.org>\n"
"Language: fr\n"
"Language-Team: support@mentors.debian.net\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: debexpo/controllers/login.py:84 debexpo/controllers/login.py:88
msgid "Invalid email or password"
msgstr "Courriel ou mot de passe incorrect"

#: debexpo/controllers/my.py:299 debexpo/templates/my/index.mako:178
#: debexpo/templates/my/index.mako:277 debexpo/templates/my/index.mako:283
msgid "None"
msgstr "Aucun(e)"

#: debexpo/controllers/my.py:300
msgid "Email"
msgstr "Courriel"

#: debexpo/controllers/my.py:301
msgid "IRC"
msgstr "IRC"

#: debexpo/controllers/my.py:302
msgid "Jabber"
msgstr "Jabber"

#: debexpo/controllers/package.py:105
msgid "Unreviewed"
msgstr "Non revu"

#: debexpo/controllers/package.py:106 debexpo/templates/package/index.mako:182
msgid "Needs work"
msgstr "Corrections nécessaires"

#: debexpo/controllers/package.py:107 debexpo/templates/package/index.mako:186
msgid "Perfect"
msgstr "Parfait"

#: debexpo/controllers/package.py:173
msgid "No subscription"
msgstr "Pas d'abonnement"

#: debexpo/controllers/package.py:175
msgid "Package upload notifications only"
msgstr "Uniquement les notifications d'envoi de paquets"

#: debexpo/controllers/package.py:177
msgid "Package upload and comment notifications"
msgstr "Notification pour les commentaires et les envois de paquets"

#: debexpo/controllers/packages.py:118
msgid "Today"
msgstr "Aujourd'hui"

#: debexpo/controllers/packages.py:121
msgid "Yesterday"
msgstr "Hier"

#: debexpo/controllers/packages.py:125
msgid "Some days ago"
msgstr "Il y a quelques jours"

#: debexpo/controllers/packages.py:129
msgid "Older packages"
msgstr "Paquets plus anciens"

#: debexpo/controllers/packages.py:133
msgid "Uploaded long ago"
msgstr "Envoyé il y a longtemps"

#: debexpo/controllers/packages.py:156
#, python-format
msgid "%s packagesdebexpo.sitename"
msgstr "%s packagesdebexpo.sitename"

#: debexpo/controllers/packages.py:158
#, python-format
msgid "A feed of packages on %sdebexpo.sitename"
msgstr "Le flux des paquets sur %sdebexpo.sitename"

#: debexpo/controllers/packages.py:181
#, python-format
msgid "Package %s uploaded by %s."
msgstr "Paquet %s envoyé par %s."

#: debexpo/controllers/packages.py:186
msgid "Uploader is currently looking for a sponsor."
msgstr "Le téléverseur recherche un parrain."

#: debexpo/controllers/packages.py:188
msgid "Uploader is currently not looking for a sponsor."
msgstr "Le téléverseur ne recherche pas de parrain."

#: debexpo/controllers/password_recover.py:74
msgid "We do not have an account with that email address"
msgstr "Nous n'avons pas de compte avec cette adresse"

#: debexpo/controllers/sponsor.py:69
msgid "Overview"
msgstr "Présentation"

#: debexpo/controllers/sponsor.py:71
msgid "Sponsoring Guidelines"
msgstr "Recommandation pour le parrainage"

#: debexpo/controllers/sponsor.py:72
msgid "Request for Sponsorship"
msgstr "Demande de parrainage"

#: debexpo/lib/validators.py:76 debexpo/lib/validators.py:91
msgid "Invalid GPG key"
msgstr "Clef GPG non valide"

#: debexpo/lib/validators.py:80
msgid "Internal error: debexpo is not properly configured to handleGPG keys"
msgstr ""
"Erreur interne : debexpo n'est pas configuré correctement pour traiter "
"les clefs GPG"

#: debexpo/lib/validators.py:87
msgid "Multiple keys not supported"
msgstr "Multiple clefs GPG non pris en charge"

#: debexpo/lib/validators.py:108
#, python-format
msgid "None of your user IDs in key %s does match your profile mail address"
msgstr ""
"Aucun des identifiants utilisateur de votre clef GPG %s ne correspond à "
"l'adresse de courriel de votre profil"

#: debexpo/lib/validators.py:124
#, python-format
msgid ""
"Key strength unacceptable in Debian Keyring. The minimum required key "
"strength is %s bits."
msgstr ""
"Cette taille de clef n'est pas acceptable dans le trousseau de clefs de "
"Debian. La taille minimale requise est de %s bits."

#: debexpo/lib/validators.py:130
msgid "Key type unacceptable in Debian Keyring. It must be RSA or ECDSA."
msgstr ""
"Ce type de clef n'est pas acceptable dans le trousseau de clefs de "
"Debian. Elle doit être de type RSA ou ECDSA."

#: debexpo/lib/validators.py:153
msgid "Incorrect password"
msgstr "Mot de passe incorrect"

#: debexpo/lib/validators.py:191
msgid "A user with this email address is already registered on the system"
msgstr "Un utilisateur avec cette adresse de courriel existe déjà sur ce site"

#: debexpo/lib/validators.py:223
msgid ""
"A user with this name is already registered on the system. If it is you, "
"use that account! Otherwise use a different name to register."
msgstr ""
"Un utilisateur avec ce nom existe déjà sur ce site. Si ce compte vous "
"appartient, utilisez-le ! Sinon, merci de choisir un nom différent pour "
"la création de votre compte."

#: debexpo/templates/base.mako:33
msgid "Start page"
msgstr "Page d'accueil"

#: debexpo/templates/base.mako:38 debexpo/templates/packages/index.mako:4
msgid "Package list"
msgstr "Liste des paquets"

#: debexpo/templates/base.mako:43 debexpo/templates/user.mako:10
msgid "Maintainer"
msgstr "Mainteneur"

#: debexpo/templates/base.mako:48
msgid "Reviews"
msgstr "Revues"

#: debexpo/templates/base.mako:53
msgid "Sponsors"
msgstr "Parrains"

#: debexpo/templates/base.mako:58 debexpo/templates/index/contact.mako:6
msgid "Q & A"
msgstr "FAQ"

#: debexpo/templates/base.mako:63 debexpo/templates/base.mako:113
msgid "Contact"
msgstr "Contact"

#: debexpo/templates/base.mako:81
msgid "Sign me up"
msgstr "Créer mon compte"

#: debexpo/templates/base.mako:84 debexpo/templates/login/index.mako:4
#: debexpo/templates/login/index.mako:7
msgid "Login"
msgstr "Se connecter"

#: debexpo/templates/base.mako:88 debexpo/templates/my/index.mako:4
msgid "My account"
msgstr "Mon compte"

#: debexpo/templates/base.mako:91
msgid "My packages"
msgstr "Mes paquets"

#: debexpo/templates/base.mako:107
msgid "Source code and bugs"
msgstr "Code source et bogues"

#: debexpo/templates/base.mako:109 debexpo/templates/package/index.mako:89
#: debexpo/templates/packages/list.mako:14
msgid "Version"
msgstr "Version"

#: debexpo/templates/error.mako:6 debexpo/templates/error.mako:11
#: debexpo/templates/error.mako:16
msgid "Error"
msgstr "Erreur"

#: debexpo/templates/error.mako:6
msgid "Authentication required"
msgstr "Authentification requise"

#: debexpo/templates/error.mako:8
msgid "You need to be logged in to use this function."
msgstr "Vous devez vous connecter pour utiliser cette fonctionnalité"

#: debexpo/templates/error.mako:11
msgid "Not authorized"
msgstr "Non autorisé"

#: debexpo/templates/error.mako:13
msgid "You do not have the permission to use this function."
msgstr "Vous n'avez pas la permission d'utiliser cette fonctionnalité"

#: debexpo/templates/error.mako:16
msgid "Page not found"
msgstr "Page non trouvée"

#: debexpo/templates/error.mako:18
msgid "The page you requested does not exist."
msgstr "La page que vous avez demandée n'existe pas."

#: debexpo/templates/error.mako:21
#, python-format
msgid "Internal error %s"
msgstr "Erreur interne %s"

#: debexpo/templates/error.mako:23
msgid ""
"An unexpected error occured in this application.\n"
"            The administrator will get a detailed report about the\n"
"            error situation. We appreciate if you give us more\n"
"            information how this error situation happened."
msgstr ""
"Une erreur inattendue est survenue dans cette application.\n"
"            Les administrateurs vont recevoir un rapport détaillé "
"indiquant\n"
"            la nature exacte de l'erreur. N'hésitez pas à nous envoyer "
"plus\n"
"            d'informations sur comment cette erreur s'est produite."

#: debexpo/templates/error.mako:33
#, python-format
msgid ""
"If you have questions feel free to contact us\n"
"        at <a href=\"mailto:%s\">%s</a>."
msgstr ""
"Si vous avez des questions, n'hésitez pas à nous contacter\n"
"        à <a href=\"mailto:%s\">%s</a>."

#: debexpo/templates/my/index.mako:141 debexpo/templates/user.mako:6
msgid "Debian Developer"
msgstr "Développeur Debian"

#: debexpo/templates/my/index.mako:148 debexpo/templates/user.mako:8
msgid "Debian Maintainer"
msgstr "Mainteneur Debian"

#: debexpo/templates/user.mako:35
msgid "Not specified"
msgstr "Non spécifié"

#: debexpo/templates/email/comment_posted.mako:3
#, python-format
msgid "Comment posted on %s"
msgstr "Commentaire posté sur %s"

#: debexpo/templates/email/comment_posted.mako:7
#, python-format
msgid ""
"A comment has been posted on a package that you are subscribed to.\n"
"\n"
"%s made the following comment about the %s package:"
msgstr ""
"Un commentaire a été posté à propos d’un paquet auquel vous êtes abonné.\n"
"\n"
"%s a écrit le commentaire suivant à propos du paquet %s :"

#: debexpo/templates/email/comment_posted.mako:11
#: debexpo/templates/email/package_uploaded.mako:7
msgid "You can view information on the package by visiting:"
msgstr "Vous pouvez consulter les informations sur ce paquet en visitant :"

#: debexpo/templates/email/comment_posted.mako:15
#: debexpo/templates/email/package_uploaded.mako:11
msgid "You can change your subscription by visiting your user account settings."
msgstr ""
"Vous pouvez mettre à jour votre abonnement en accédant aux paramètres de "
"votre compte utilisateur."

#: debexpo/templates/email/comment_posted.mako:17
#: debexpo/templates/email/importer_fail_admin.mako:14
#: debexpo/templates/email/importer_fail_maintainer.mako:14
#: debexpo/templates/email/package_uploaded.mako:13
#: debexpo/templates/email/password_reset.mako:12
#: debexpo/templates/email/register_activate.mako:12
#: debexpo/templates/email/successful_upload.mako:28
#: debexpo/templates/email/upload_removed_from_expo.mako:9
msgid "Thanks,"
msgstr "Merci,"

#: debexpo/templates/email/importer_fail_admin.mako:3
msgid "Failure in importer"
msgstr "Échec de l'importation"

#: debexpo/templates/email/importer_fail_admin.mako:5
msgid ""
"Hello,\n"
"\n"
"There was a failure in importing a package into debexpo. The problem\n"
"appears to be debexpo itself. The error message was:"
msgstr ""
"Bonjour,\n"
"\n"
"Un problème est survenu lors de l'importation du paquet dans debexpo.\n"
"Le problème semble venir de debexpo lui-même. Le message d'erreur est :"

#: debexpo/templates/email/importer_fail_admin.mako:12
msgid "This message can be found in the logs."
msgstr "Ce message est disponible dans le journal d'événements"

#: debexpo/templates/email/importer_fail_maintainer.mako:3
#, python-format
msgid "Failed to import %s"
msgstr "Échec de l'importation de %s"

#: debexpo/templates/email/importer_fail_maintainer.mako:5
#, python-format
msgid ""
"Hello,\n"
"\n"
"There was a failure in importing your package \"%s\" into\n"
"the repository. The problem appears to be in the repository software\n"
"and not your package.\n"
"\n"
"Sorry for the inconvenience. The administrator has been notified.\n"
"Please try again soon."
msgstr ""
"Bonjour,\n"
"\n"
"Un problème est survenu lors de l'importation du paquet dans le dépôt.\n"
"Le problème semble venir du logiciel de gestion du dépôt\n"
"et non de votre paquet.\n"
"\n"
"Nous sommes désolés de la gêne occasionnée. Les administrateurs ont été "
"avertis\n"
"Merci de réessayer prochainement."

#: debexpo/templates/email/importer_reject_maintainer.mako:3
#, python-format
msgid "Package %s was rejected"
msgstr "Le paquet %s a été rejeté"

#: debexpo/templates/email/importer_reject_maintainer.mako:5
#, python-format
msgid ""
"Hello,\n"
"\n"
"Unfortunately your package \"%s\" was rejected because of the following\n"
"reason:"
msgstr ""
"Bonjour,\n"
"\n"
"Malheureusement, votre paquet \"%s\" a été rejeté pour la raison\n"
"suivante :"

#: debexpo/templates/email/importer_reject_maintainer.mako:12
msgid "Please try to fix it and re-upload. Thanks,"
msgstr "Merci d'essayer de le corriger et de l'envoyer à nouveau."

#: debexpo/templates/email/package_uploaded.mako:3
#, python-format
msgid "New %s package uploaded"
msgstr "Nouveau paquet %s envoyé"

#: debexpo/templates/email/package_uploaded.mako:5
#, python-format
msgid "%s %s has been uploaded to the archive by %s."
msgstr "%s %s a été envoyé dans l'archive par %s."

#: debexpo/templates/email/password_reset.mako:3
msgid "You requested a password reset"
msgstr "Vous avez demandé une réinitialisation de mot de passe"

#: debexpo/templates/email/password_reset.mako:5
msgid ""
"Hello,\n"
"\n"
"You can reset your password by clicking on the link below. If you did\n"
"not request a password reset, then you can ignore this."
msgstr ""
"Bonjour,\n"
"\n"
"Vous pouvez réinitialiser votre mot de passe en cliquant sur le lien\n"
"ci-dessous. Si vous n'avez pas demandé cette réinitialisation, vous "
"pouvez\n"
"ignorer ce message."

#: debexpo/templates/email/register_activate.mako:3
msgid "Next step: Confirm your email address"
msgstr "Prochaine étape : confirmation de votre adresse de courriel"

#: debexpo/templates/email/register_activate.mako:5
msgid ""
"Hello,\n"
"\n"
"Please activate your account by visiting the following address\n"
"in your web-browser:"
msgstr ""
"Bonjour,\n"
"\n"
"Merci d'activer votre compte en visitant l'adresse suivante dans votre\n"
"navigateur :"

#: debexpo/templates/email/successful_upload.mako:3
#, python-format
msgid "%s uploaded to %s"
msgstr "%s envoyé sur %s"

#: debexpo/templates/email/successful_upload.mako:5
msgid "Hi."
msgstr "Bonjour."

#: debexpo/templates/email/successful_upload.mako:9
#, python-format
msgid ""
"Your upload of the package '%s' to %s was\n"
"successful. Others can now see it. The URL of your package is:"
msgstr ""
"L'envoi du paquet '%s' sur %s a réussi.\n"
"Il est maintenant visible pour tous. L'URL de votre paquet est :"

#: debexpo/templates/email/successful_upload.mako:12
msgid "The respective dsc file can be found at:"
msgstr "Le fichier dsc respectif peut être trouvé sur :"

#: debexpo/templates/email/successful_upload.mako:16
#, python-format
msgid ""
"If you do not yet have a sponsor for your package you may want to go to\n"
"%s\n"
"and set the \"Seeking a sponsor\" option to highlight your package on the"
"\n"
"welcome page."
msgstr ""
"Si vous n'avez pas encore trouvé de parrain pour votre paquet, vous "
"pourriez\n"
"être intéressé pour aller sur %s\n"
"et activer l'option \"Recherche de parrain\" pour mettre en avant votre "
"paquet\n"
"sur la page d'accueil."

#: debexpo/templates/email/successful_upload.mako:21
msgid ""
"You can also send an RFS (request for sponsorship) to the debian-mentors\n"
"mailing list. Your package page will give your suggestions on how to\n"
"send that mail."
msgstr ""
"Vous pouvez également envoyer une RFS (request for sponsorship) sur la\n"
"liste de diffusion list debian-mentors. La page de votre paquet contient\n"
"des informations sur comment envoyer ce courriel."

#: debexpo/templates/email/successful_upload.mako:25
msgid "Good luck in finding a sponsor!"
msgstr "Bonne chance dans votre recherche de parrainage !"

#: debexpo/templates/email/upload_removed_from_expo.mako:3
#, python-format
msgid "%s package has been removed from %s"
msgstr "Le paquet %s a été supprimé de %s"

#: debexpo/templates/email/upload_removed_from_expo.mako:5
#, python-format
msgid "Your package %s %s has been removed from %s for the following reason:"
msgstr "Votre paquet %s %s a été supprimé de %s pour la raison suivante :"

#: debexpo/templates/index/contact.mako:4
msgid "Site contact"
msgstr "Contact du site"

#: debexpo/templates/index/contact.mako:6
msgid "file a RFS bug"
msgstr "remplir un bogue RFS"

#: debexpo/templates/index/contact.mako:8 debexpo/templates/index/qa.mako:50
msgid "Site email"
msgstr "Courriel du site"

#: debexpo/templates/index/reviewers.mako:35
msgid "sponsors side"
msgstr "coté des parrains"

#: debexpo/templates/login/index.mako:9
msgid "Please login to continue"
msgstr "Merci de vous connecter pour continuer"

#: debexpo/templates/login/index.mako:19 debexpo/templates/my/index.mako:19
#: debexpo/templates/register/register.mako:17
msgid "E-mail"
msgstr "Courriel"

#: debexpo/templates/login/index.mako:24
#: debexpo/templates/register/register.mako:22
msgid "Password"
msgstr "Mot de passe"

#: debexpo/templates/login/index.mako:29 debexpo/templates/my/index.mako:24
#: debexpo/templates/my/index.mako:66 debexpo/templates/my/index.mako:98
#: debexpo/templates/my/index.mako:161 debexpo/templates/my/index.mako:293
#: debexpo/templates/package/index.mako:237
#: debexpo/templates/package/subscribe.mako:9
#: debexpo/templates/password_recover/index.mako:29
#: debexpo/templates/register/register.mako:39
msgid "Submit"
msgstr "Envoyer"

#: debexpo/templates/login/index.mako:36
msgid "Switch to SSL"
msgstr "Passer en SSL"

#: debexpo/templates/login/index.mako:39
msgid "Did you lose your password?"
msgstr "Vous avez oublié votre mot de passe ?"

#: debexpo/templates/login/index.mako:41
msgid "Try resetting your password."
msgstr "Essayez de réinitialiser votre mot de passe."

#: debexpo/templates/my/index.mako:7
msgid "Change details"
msgstr "Modification des détails"

#: debexpo/templates/my/index.mako:14 debexpo/templates/package/index.mako:8
msgid "Name"
msgstr "Nom"

#: debexpo/templates/my/index.mako:33
msgid "Change GPG key"
msgstr "Modification de la clef GPG"

#: debexpo/templates/my/index.mako:43
msgid "Current GPG key"
msgstr "Clef GPG actuelle"

#: debexpo/templates/my/index.mako:48
msgid "Delete current key"
msgstr "Supprimer la clef GPG actuelle"

#: debexpo/templates/my/index.mako:59
msgid "GPG key"
msgstr "Clef GPG"

#: debexpo/templates/my/index.mako:76
msgid "Change password"
msgstr "Modification du mot de passe"

#: debexpo/templates/my/index.mako:83
msgid "Current password"
msgstr "Mot de passe actuel"

#: debexpo/templates/my/index.mako:88
msgid "New password"
msgstr "Nouveau mot de passe"

#: debexpo/templates/my/index.mako:93
msgid "Confirm new password"
msgstr "Confirmez le nouveau mot de passe"

#: debexpo/templates/my/index.mako:107
msgid "Change other details"
msgstr "Modification d'autres détails"

#: debexpo/templates/my/index.mako:114
msgid "Country"
msgstr "Pays"

#: debexpo/templates/my/index.mako:119
msgid "IRC nickname"
msgstr "pseudo IRC"

#: debexpo/templates/my/index.mako:124
msgid "Jabber address"
msgstr "Adresse Jabber"

#: debexpo/templates/my/index.mako:130
msgid "Show personal data publicly"
msgstr "Afficher les données personnelles publiquement"

#: debexpo/templates/my/index.mako:140
msgid "Debian status"
msgstr "Statut Debian"

#: debexpo/templates/my/index.mako:169
msgid "Public sponsor info"
msgstr "Information publique sur le parrain"

#: debexpo/templates/my/index.mako:176
msgid "Public visibility of your profile"
msgstr "Visibilité publique de votre profil"

#: debexpo/templates/my/index.mako:179
msgid "Restricted"
msgstr "Restreinte"

#: debexpo/templates/my/index.mako:180
msgid "Full"
msgstr "Complète"

#: debexpo/templates/my/index.mako:191
msgid "Preferred contact method for sponsored maintainer"
msgstr "Méthode de contact préférée pour les mainteneurs parrainés"

#: debexpo/templates/my/index.mako:196
msgid "Type of packages you are interested in"
msgstr "Types de paquets par lesquels vous êtes intéressé"

#: debexpo/templates/my/index.mako:219 debexpo/templates/my/index.mako:258
msgid "-"
msgstr "-"

#: debexpo/templates/my/index.mako:220 debexpo/templates/my/index.mako:259
msgid "0"
msgstr "0"

#: debexpo/templates/my/index.mako:221 debexpo/templates/my/index.mako:260
msgid "+"
msgstr "+"

#: debexpo/templates/my/index.mako:234
msgid "Additional social notes"
msgstr "Notes sociales additionnelles"

#: debexpo/templates/my/index.mako:273
msgid "Additional technical notes"
msgstr "Notes techniques additionnelles"

#: debexpo/templates/my/index.mako:278 debexpo/templates/my/index.mako:284
msgid "Free text"
msgstr "Texte libre"

#: debexpo/templates/my/index.mako:279 debexpo/templates/my/index.mako:285
msgid "URL reference"
msgstr "URL de référence"

#: debexpo/templates/package/index.mako:4
#, python-format
msgid "Details about package \"%s\""
msgstr "Détails à propos du paquet \"%s\""

#: debexpo/templates/package/index.mako:23
#: debexpo/templates/packages/list.mako:15
msgid "Uploader"
msgstr "Téléverseur"

#: debexpo/templates/package/index.mako:36
#: debexpo/templates/packages/list.mako:13
msgid "Description"
msgstr "Description"

#: debexpo/templates/package/index.mako:43
msgid "Subscribe"
msgstr "S'abonner"

#: debexpo/templates/package/index.mako:44
msgid "Edit your subscription"
msgstr "Modifier votre abonnement"

#: debexpo/templates/package/index.mako:49
#: debexpo/templates/packages/list.mako:16
msgid "Needs a sponsor"
msgstr "Recherche un parrain"

#: debexpo/templates/package/index.mako:52
#: debexpo/templates/packages/list.mako:27
msgid "Yes"
msgstr "Oui"

#: debexpo/templates/package/index.mako:54
#: debexpo/templates/packages/list.mako:29
msgid "No"
msgstr "Non"

#: debexpo/templates/package/index.mako:57
msgid "Change"
msgstr "Modifier"

#: debexpo/templates/package/index.mako:67
msgid "Delete package"
msgstr "Supprimer le paquet"

#: debexpo/templates/package/index.mako:69
msgid "Admin package deletion"
msgstr "Suppression admin du paquet"

#: debexpo/templates/package/index.mako:71
msgid "Delete this package"
msgstr "Supprimer ce paquet"

#: debexpo/templates/package/index.mako:79
msgid "Package versions"
msgstr "Versions du paquet"

#: debexpo/templates/package/index.mako:85
msgid "Information"
msgstr "Informations"

#: debexpo/templates/package/index.mako:94
msgid "View RFS template"
msgstr "Visualiser le modèle RFS"

#: debexpo/templates/package/index.mako:102
msgid "Uploaded"
msgstr "Téléversé"

#: debexpo/templates/package/index.mako:107
msgid "Source package"
msgstr "Paquet source"

#: debexpo/templates/package/index.mako:124
msgid "Section"
msgstr "Section"

#: debexpo/templates/package/index.mako:129
msgid "Priority"
msgstr "Priorité"

#: debexpo/templates/package/index.mako:138
msgid "Closes bugs"
msgstr "Fermeture des bogues"

#: debexpo/templates/package/index.mako:155
msgid "QA information"
msgstr "Information QA"

#: debexpo/templates/package/index.mako:194
msgid "Package has been uploaded to Debian"
msgstr "Le paquet a été téléversé dans Debian"

#: debexpo/templates/package/index.mako:207
msgid "No comments"
msgstr "Aucun commentaire"

#: debexpo/templates/package/index.mako:232
msgid "Uploaded to Debian"
msgstr "Téléversé dans Debian"

#: debexpo/templates/package/subscribe.mako:4
#, python-format
msgid "Subscribe to package %s"
msgstr "S'abonner au paquet %s"

#: debexpo/templates/package/subscribe.mako:12
msgid "Back to package details"
msgstr "Retour sur les détails du paquet"

#: debexpo/templates/packages/list.mako:12
msgid "Package"
msgstr "Paquet"

#: debexpo/templates/packages/list.mako:39
msgid "No packages"
msgstr "Aucun paquet"

#: debexpo/templates/packages/maintainer.mako:4
#, python-format
msgid "Packages maintained by %s"
msgstr "Paquets maintenus par %s"

#: debexpo/templates/packages/section.mako:4
#, python-format
msgid "Packages in section %s"
msgstr "Paquets dans la section %s"

#: debexpo/templates/packages/uploader.mako:6
#, python-format
msgid "Packages uploaded by %s"
msgstr "Paquets téléversés par %s"

#: debexpo/templates/password_recover/_reset.mako:4
msgid "Password recovery in progress"
msgstr "Réinitialisation du mot de passe en cours"

#: debexpo/templates/password_recover/actually_reset_password.mako:4
msgid "Okay! You have a new password"
msgstr "OK ! Vous avez un nouveau mot de passe"

#: debexpo/templates/password_recover/index.mako:4
msgid "Password recovery"
msgstr "Réinitialisation du mot de passe"

#: debexpo/templates/password_recover/index.mako:24
msgid "Your email address"
msgstr "Votre adresse de courriel"

#: debexpo/templates/register/activate.mako:5
msgid "Check your email"
msgstr "Vérifiez votre boite aux lettres"

#: debexpo/templates/register/activate.mako:8
msgid ""
"An email has been sent to the email address you specified. Check it for "
"instructions on how to activate your account."
msgstr ""
"Un courriel a été envoyé à l'adresse que vous avez spécifiée. Vous y "
"trouverez toutes les instructions sur comment activer votre compte."

#: debexpo/templates/register/activated.mako:7
msgid "User activated"
msgstr "Utilisateur activé"

#: debexpo/templates/register/activated.mako:10
msgid "Your account has been activated."
msgstr "Votre compte a été activé."

#: debexpo/templates/register/activated.mako:11
#, python-format
msgid "You can now %sproceed to login%s."
msgstr "Vous pouvez maintenant %svous connecter%s."

#: debexpo/templates/register/activated.mako:16
msgid "Invalid verification key"
msgstr "Clef de vérification non valable"

#: debexpo/templates/register/activated.mako:19
msgid ""
"The verification key you entered has not been recognised. Please try "
"again."
msgstr ""
"La clef de vérification que vous utilisez n'est pas reconnue. Merci de "
"réessayer"

#: debexpo/templates/register/register.mako:4
msgid "Sign up for an account"
msgstr "Créez un compte"

#: debexpo/templates/register/register.mako:7
msgid "Account details"
msgstr "Détails du compte"

#: debexpo/templates/register/register.mako:12
msgid "Full name"
msgstr "Nom complet"

#: debexpo/templates/register/register.mako:27
msgid "Confirm password"
msgstr "Confirmez le mot de passe"

#: debexpo/templates/register/register.mako:32
msgid "Account type"
msgstr "Type de compte"

#: debexpo/templates/sponsor/guidelines.mako:13
msgid "Filter"
msgstr "Filtre"

#: debexpo/templates/sponsor/guidelines.mako:20
msgid "Add to filter"
msgstr "Ajouter au filtre"

#: debexpo/templates/sponsor/guidelines.mako:26
msgid "Remove filter"
msgstr "Supprimer le filtre"

#: debexpo/templates/sponsor/guidelines.mako:71
msgid "Store filter as default"
msgstr "Sauvegarder le filtre en tant que défaut"

#: debexpo/templates/sponsor/guidelines.mako:72
#: debexpo/templates/sponsor/guidelines.mako:192
msgid "Remove all filters"
msgstr "Supprimer tous les filtres"

#: debexpo/templates/sponsor/guidelines.mako:182
msgid "No sponsor matched your criteria"
msgstr "Aucun parrain ne correspond à vos critères"

#: debexpo/templates/sponsor/guidelines.mako:186
msgid "No sponsors found"
msgstr "Aucun parrain trouvé"

#: debexpo/templates/sponsor/rfs_howto.mako:34
msgid "Template for an RFS bug"
msgstr "Modèle pour un bogue RFS"

#: debexpo/templates/sponsor/rfs_howto.mako:36
#, python-format
msgid "for \"%s\""
msgstr "pour \"%s\""

#: debexpo/templates/sponsor/rfs_howto.mako:183
msgid "a dedicated page"
msgstr "une page dédiée"

